#!/bin/sh

# Fail if there are no options provided
test -n "$ACCESS_LOGS_FLAGS" || exit 1

# Give write access to Nginx
umask 007

pid=/run/access_logd.pid

case $1 in
  start)
    # Copy default database
    install -m 640 -o nobody /var/lib/access_log.sqlite3 /var/log/access_log.sqlite3

    # Remove socket if previous run ended suddendly
    rm -f /tmp/access_log.socket

    # Read from fifo and load into database
    daemonize -p /run/access_logd.pid -u nginx /usr/local/bin/syslogize /usr/bin/access_log $ACCESS_LOGS_FLAGS
  ;;
  stop)
    test -f $pid || exit 0

    cat $pid | xargs kill
    rm $pid
  ;;
esac
