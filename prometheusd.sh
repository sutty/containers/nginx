#!/bin/sh
set -e

pid=/tmp/prometheus.pid

case $1 in
  start) 
    daemonize -c /tmp -p ${pid} -l ${pid} -u nobody \
      /usr/local/bin/syslogize \
      /usr/bin/nginx-prometheus-exporter -nginx.scrape-uri http://127.0.0.1:8080/stub_status

    ;;
  stop)
    test ! -f ${pid} || cat ${pid} | xargs -r kill
    rm -f ${pid}
    ;;
esac
