ARG ALPINE_VERSION=3.13.6
ARG BASE_IMAGE=sutty/monit
FROM ${BASE_IMAGE}:${ALPINE_VERSION}
MAINTAINER "f <f@sutty.nl>"

ARG ACCESS_LOGS_FLAGS="--database=sqlite3:///var/log/access_log.sqlite3 -c /usr/share/crawler-user-agents/crawler-user-agents.json"
ARG ACCESS_LOG_VERSION="0.5.1"

ENV ACCESS_LOGS_FLAGS=${ACCESS_LOGS_FLAGS}

# Install nginx and remove default config
RUN apk add --no-cache nginx daemonize access_log~${ACCESS_LOG_VERSION} nginx-prometheus-exporter geoip2-database-country geoip2-database-city crawler-user-agents \
    && rm -rf /etc/nginx

# Add ssl group so nginx has access to certificates
RUN addgroup -S -g 777 ssl
RUN addgroup nginx ssl

COPY ./monit.conf /etc/monit.d/nginx.conf
COPY ./prometheusd.sh /usr/local/bin/prometheusd
COPY ./access_logd.sh /usr/bin/access_logd
COPY ./nginx /etc/nginx
COPY ./access_log.sqlite3 /var/lib/access_log.sqlite3

# Install modules after rewriting /etc/nginx
RUN apk add --no-cache nginx-mod-http-brotli nginx-mod-http-geoip2

# Add support for request_uri parsing if access_log >= 0.3.0
RUN mv /etc/nginx/access_logd_`access_log -v`.conf /etc/nginx/access_logd.conf
RUN chown -R nginx:nginx /etc/nginx
RUN nginx -t

RUN chown nginx:nginx /var/lib/access_log.sqlite3

# Shared configuration
VOLUME /etc/nginx/sites
VOLUME /etc/letsencrypt
VOLUME /var/lib/letsencrypt

EXPOSE 80
EXPOSE 443
EXPOSE 9113
